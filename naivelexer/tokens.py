import re


class UnknownLiteral(Exception):
    pass


class TokenRegistry(type):

    registered_tokens = []

    def __new__(mcls, kls_name, bases, attrs):
        pattern = attrs.get('pattern')
        new_kls = type.__new__(mcls, kls_name, bases, attrs)
        if pattern:
            mcls.registered_tokens.append(new_kls)
        return new_kls

    @classmethod
    def from_literal(kls, literal):
        for token_kls in kls.registered_tokens:
            if re.match(token_kls.pattern, literal):
                return token_kls(literal)
        raise UnknownLiteral(literal)


class Token(object):
    __metaclass__ = TokenRegistry
    builder = str

    def __init__(self, value=None):
        self.value = self.builder(value)

    def __eq__(self, other):
        return type(self) == type(other) and self.value == other.value


class Operator(Token):

    def __eq__(self, other):
        return isinstance(other, type(self))


class NumberToken(Token):
    pattern = "[0-9]+"
    builder = int


class PlusToken(Operator):
    pattern = "\+"


class MinusToken(Operator):
    pattern = "-"


class MultiplyToken(Operator):
    pattern = "\*"


class DivisionToken(Operator):
    pattern = "/"


class DotToken(Operator):
    pattern = "\."


class PowerToken(Operator):
    pattern = "\^"


class EOFToken(Token):
    def __eq__(self, other):
        return isinstance(other, type(self))
