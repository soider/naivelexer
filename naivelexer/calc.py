import naivelexer.tokens as tokens


class WrongEatToken(Exception): pass


class Calc(object):

    def __init__(self, analyzer):
        self.analyzer = analyzer
        self.current_token = None

    def result(self):
        self.current_token = self.analyzer.next_token()
        return self.expression()

    def expression(self):
        result = self.term()
        while isinstance(self.current_token, (tokens.PlusToken, tokens.MinusToken)):
            token = self.eat(tokens.Operator)
            if isinstance(token, tokens.PlusToken):
                result += self.term()
            else:
                result -= self.term()
        self.eat(tokens.EOFToken)
        return result

    def eat(self, token_type):
        token = self.current_token
        if not isinstance(token, token_type):
            raise WrongEatToken("Have %s, expected %s", type(token), token_type)
        else:
            self.current_token = self.analyzer.next_token()
            return token

    def term(self):
        result = self.eat(tokens.NumberToken).value
        while isinstance(self.current_token,
                            (tokens.MultiplyToken, tokens.DivisionToken, tokens.PowerToken)):
            token = self.eat(tokens.Operator)
            if isinstance(token, tokens.MultiplyToken):
                result *= self.term()
            elif isinstance(token, tokens.DivisionToken):
                result /= self.term()
            else:
                result = pow(result, self.term())
        return result

def main():
    from naivelexer import analyzer
    while True:
        text = raw_input("> ")
        print Calc(analyzer.LexicalAnalyzer(text)).result()

if __name__ == "__main__":
    main()
