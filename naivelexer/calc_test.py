import pytest

from . import tokens, analyzer, calc


def test_do_expression_simple():
    lex = analyzer.LexicalAnalyzer("1 + 2")
    assert calc.Calc(lex).result() == 3


def test_do_expression_simple_wrong():
    lex = analyzer.LexicalAnalyzer("1 + ")
    with pytest.raises(calc.WrongEatToken):
        calc.Calc(lex).result()


def test_do_expression_first_order():
    lex = analyzer.LexicalAnalyzer("1 + 2 - 4 + 5")
    assert calc.Calc(lex).result() == 4


def test_do_expression_first_order_plus():
    lex = analyzer.LexicalAnalyzer("1 + 2 + 4 + 5")
    assert calc.Calc(lex).result() == 12


def test_do_expression_second_order_mult():
    lex = analyzer.LexicalAnalyzer("1 * 2 * 3")
    assert calc.Calc(lex).result() == 6


def test_do_expression_second_order_div():
    lex = analyzer.LexicalAnalyzer("1 * 6 / 3")
    assert calc.Calc(lex).result() == 2


def test_do_expression_priority():
    lex = analyzer.LexicalAnalyzer("1 + 2 * 3")
    assert calc.Calc(lex).result() == 7


def test_do_power_expression():
    lex = analyzer.LexicalAnalyzer("3 ^ 2")
    assert calc.Calc(lex).result() == 9
