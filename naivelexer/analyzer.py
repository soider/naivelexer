import re

import tokens


class LexicalAnalyzer(object):

    def __init__(self, text):
        self.text = text.strip()
        self.last_position = len(self.text) - 1
        self.current_position = 0

    @property
    def current_char(self):
        return self.text[self.current_position]

    def increment(self):
        self.current_position += 1

    def decrement(self):
        self.current_position -= 1

    def match_operator(self):
        if not self.match_space():
            return isinstance(tokens.TokenRegistry.from_literal(self.current_char),
                          tokens.Operator)

    def match_digit(self):
        return self.current_char.isdigit()

    def match_space(self):
        return self.current_char.isspace()

    def skip_spaces(self):
        while self.match_space():
            self.increment()

    def next_token(self):
        if self.current_position > self.last_position:
            return tokens.EOFToken()
        self.skip_spaces()
        if self.match_operator():
            token = tokens.TokenRegistry.from_literal(self.current_char)
            self.increment()
            return token
        accum = []
        while self.match_digit():
            accum.append(self.current_char)
            self.increment()
            if self.current_position > self.last_position:
                break
        return tokens.TokenRegistry.from_literal(''.join(accum))
