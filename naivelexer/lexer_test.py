import pytest

from . import analyzer, tokens


def test_extracting_number():
    lex = analyzer.LexicalAnalyzer('12345612')
    token = lex.next_token()
    assert token == tokens.NumberToken(12345612)


def test_extracting_two_numbers():
    digits = [12, 34]
    lex = analyzer.LexicalAnalyzer('  12    34   ')
    assert lex.next_token() == tokens.NumberToken(digits[0])
    assert lex.next_token() == tokens.NumberToken(digits[1])


def test_extracting_three_numbers():
    digits = [10, 12, 14]
    lex = analyzer.LexicalAnalyzer('10  12  14')
    assert lex.next_token() == tokens.NumberToken(digits[0])
    assert lex.next_token() == tokens.NumberToken(digits[1])
    assert lex.next_token() == tokens.NumberToken(digits[2])


def test_extracting_end_of_file():
    lex = analyzer.LexicalAnalyzer('12345612')
    lex.next_token()
    token = lex.next_token()
    assert token == tokens.EOFToken()


def test_extracting_operators():
    assert analyzer.LexicalAnalyzer('+').next_token() == tokens.PlusToken()
    assert analyzer.LexicalAnalyzer('-').next_token() == tokens.MinusToken()
    assert analyzer.LexicalAnalyzer('*').next_token() == tokens.MultiplyToken()
    assert analyzer.LexicalAnalyzer('/').next_token() == tokens.DivisionToken()


def test_extracting_operators_in_a_row():
    lex = analyzer.LexicalAnalyzer('+     +')
    assert lex.next_token() == tokens.PlusToken()
    assert lex.next_token() == tokens.PlusToken()


def test_skip_spaces():
    space_length = 10
    pattern = '+%s5' % (" " * space_length)
    stash_position = pattern.find('5')
    lex = analyzer.LexicalAnalyzer(pattern)
    lex.next_token()
    lex.skip_spaces()
    assert lex.current_position == stash_position


def test_operator_from_literal():
    assert tokens.Token.from_literal('+') == tokens.PlusToken()
    assert tokens.Token.from_literal('-') == tokens.MinusToken()
    assert tokens.Token.from_literal('*') == tokens.MultiplyToken()
    assert tokens.Token.from_literal('/') == tokens.DivisionToken()
    assert tokens.Token.from_literal('.') == tokens.DotToken()


def test_number_from_literal():
    assert tokens.Token.from_literal('12341') ==\
     tokens.NumberToken(12341)


def test_invalid_literal():
    with pytest.raises(tokens.UnknownLiteral):
        tokens.Token.from_literal('@')
