"""Simple lexer"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='naivelexer',
    version='0.2',
    description='Simple regexp based lexer and examples',
    long_description=long_description,
    url='https://bitbucket.org/eithz/naivelexer',
    author='Mikhail Sakhnov',
    author_email='sahnov.m@gmail.com',
    license='MIT',
    # What does your project relate to?
    keywords='lexical analysis sample',
    packages=find_packages(),
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },

    entry_points={
        'console_scripts': [
            'naivelexer_calc_example=naivelexer.calc:main',
        ],
    },
)
