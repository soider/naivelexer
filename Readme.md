# Naive lexer
To add new token types simply inherit Token base class

```
class Operator(Token):

    def __eq__(self, other):
        return isinstance(other, type(self))
```

Override some methods if needed:
- `__eq__` default realization compare type and value, sometime it's overhead
- `pattern` regexp to match with
- `builder` raw string value parser, str by default


## Usage

```
lex = lexer.analyzer.LexicalAnalyzer("1 + 2 - 4 + 5")
while True:
    t = lex.next_token()
    if t == lexer.tokens.EOFToken():
        break
    print t.value
```

Outputs:
```
1
+
2
-
4
+
5
```

More examples provided in test files and calc package.
